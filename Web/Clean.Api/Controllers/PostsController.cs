using System.Threading.Tasks;
using Clean.Application.Posts.Commands.AddPost;
using Clean.Application.Posts.Queries.GetPost;
using Clean.Application.Posts.Queries.GetPosts;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;

namespace Clean.Api.Controllers
{
	public class PostsController : BaseController
	{
		[HttpGet(Name = "GetPosts")]
		[EnableQuery()]
		public async Task<IActionResult> GetAll()
		{
			return Ok(await Mediator.Send(new GetPostsQuery()));
		}

		[HttpGet("{id}", Name = "GetPost")]
		[EnableQuery()]
		public async Task<IActionResult> GetOne(int id)
		{
			return Ok(await Mediator.Send(new GetPostQuery { Id = id }));
		}

		[HttpPost(Name = "AddPost")]
		public async Task<IActionResult> AddPost([FromBody] AddPostCommand command)
		{
			return Created("api/posts/1", await Mediator.Send(command));
		}
	}
}