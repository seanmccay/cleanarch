﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Clean.Api.Filters;
using Clean.Application.Infrastructure;
using Clean.Application.Infrastructure.AutoMapper;
using Clean.Application.Interfaces;
using Clean.Application.Posts.Queries.GetPosts;
using Clean.Infrastructure;
using Clean.Persistence;
using FluentValidation.AspNetCore;
using MediatR;
using MediatR.Pipeline;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Serilog;

namespace Clean.Api
{
	public class Startup
	{
		private IHostingEnvironment _env;

		public Startup(IConfiguration configuration, IHostingEnvironment env)
		{
			Configuration = configuration;
			_env = env;
		}

		public IContainer ApplicationContainer { get; private set; }
		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			// * looks like we're able to accomplish everything we usually do with AutoFac just using built in DI
			// * I don't think we have the ability to register everything from an assembly though

			services.AddOData();

			services.AddAutoMapper(new Assembly[] { typeof(AutoMapperProfile).GetTypeInfo().Assembly });

			services.AddMediatR(typeof(GetPostsQueryHandler).GetTypeInfo().Assembly);
			services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPerformanceBehaviour<,>));
			services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
			services.AddTransient(typeof(IRequestPreProcessor<>), typeof(RequestLogger<>));

			services.AddScoped<INotificationService, NotificationService>();

			services.AddDbContext<IBlogContext, BlogContext>(options =>
				options
					.UseLazyLoadingProxies()
					.UseInMemoryDatabase("BlogDB"));

			services
				.AddMvc(options => options.Filters.Add(typeof(CustomExceptionFilterAttribute)))
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
				.AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<GetPostsQueryValidator>());

			// services.AddSingleton<ILogger>(new LoggerConfiguration().ReadFrom.ConfigurationSection(Configuration.GetSection("Serilog")).CreateLogger());

			// var builder = new ContainerBuilder();

			// * example of context registered as multiple interfaces. Can't really do with built in DI, but works with AutoFac
			// * https://andrewlock.net/how-to-register-a-service-with-multiple-interfaces-for-in-asp-net-core-di/
			// builder.Register<LivestockContext>((c) => new LivestockContext(
			// 	new DbContextOptionsBuilder<LivestockContext>()
			// 		.UseLazyLoadingProxies()
			// 		.UseSqlServer(Configuration.GetConnectionString("LivestockContext")).Options))
			// 	.As<ICattleContext>()
			// 	.As<IHorseContext>()
			// 	.As<IPigContext>()
			// 	.As<ISheepContext>()
			// 	.As<IGoatContext>()
			// 	.InstancePerLifetimeScope();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseMvc(routeBuilder =>
			{
				routeBuilder.EnableDependencyInjection();
				routeBuilder.Expand().Select().OrderBy().Filter();
			});
		}
	}
}
