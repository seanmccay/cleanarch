﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Clean.Application.Interfaces;
using Clean.Persistence;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace Clean.Api
{
	public class Program
	{
		public static void Main(string[] args)
		{
			try
			{
				var host = CreateWebHostBuilder(args).Build();

				using (var scope = host.Services.CreateScope())
				{
					try
					{
						var context = scope.ServiceProvider.GetService<IBlogContext>();

						var concreteContext = (BlogContext)context;
						// concreteContext.Database.Migrate();
						BlogContextInitializer.Initialize(concreteContext);
						Log.Information("DB Context has been seeded/initialized.");
					}
					catch (Exception ex)
					{

						Log.Error(ex, "An error occurred while migrating or initializing the database.");
					}
				}

				host.Run();
			}
			catch (Exception ex)
			{
				Log.Fatal(ex, "Host terminated unexpectedly");
			}
			finally
			{
				Log.CloseAndFlush();
			}

		}

		public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.ConfigureAppConfiguration((hostingContext, config) =>
				{
					var env = hostingContext.HostingEnvironment;
					config
						.AddEnvironmentVariables()
						.SetBasePath(env.ContentRootPath)
						.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
						.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
					// .AddJsonFile("connections.json", optional: false, reloadOnChange: true);
				})
				.UseSerilog((hostingContext, loggerConfiguration) => loggerConfiguration
					.ReadFrom.Configuration(hostingContext.Configuration)
					.Enrich.FromLogContext())
				.UseStartup<Startup>();
	}
}
