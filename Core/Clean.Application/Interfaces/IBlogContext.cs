using Microsoft.EntityFrameworkCore;
using Clean.Core.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Clean.Application.Interfaces
{
	public interface IBlogContext
	{
		DbSet<Post> Posts { get; set; }
		DbSet<User> Users { get; set; }

		Task<int> SaveChangesAsync(CancellationToken cancellationToken);
	}
}