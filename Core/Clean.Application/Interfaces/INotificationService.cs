using System.Threading.Tasks;
using Clean.Application.Models;

namespace Clean.Application.Interfaces
{
	public interface INotificationService
	{
		Task SendAsync(Message message);
	}
}
