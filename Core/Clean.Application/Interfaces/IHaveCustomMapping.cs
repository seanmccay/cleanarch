using AutoMapper;

namespace Clean.Application.Interfaces
{
	public interface IHaveCustomMapping
	{
		void CreateMappings(Profile configuration);
	}
}
