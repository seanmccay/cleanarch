using System.Collections.Generic;
using System.Threading.Tasks;
using Clean.Core.Entities;

namespace Clean.Application.Interfaces
{
	public interface IBlogService
	{
		Task<List<Post>> GetPostsAsync();
	}
}