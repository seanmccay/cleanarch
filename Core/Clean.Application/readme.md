## Application

This is where all of our Application level logic goes. 

- All exceptions not relating directly to entities
- Automapper profiles
- MediatR commands & queries (if using)
- Interfaces for services & DbContexts