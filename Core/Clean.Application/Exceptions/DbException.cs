using System;

namespace Clean.Application.Exceptions
{
	public class DbException : Exception
	{
		public DbException(string message, Exception innerException) : base(message, innerException)
		{

		}
	}
}