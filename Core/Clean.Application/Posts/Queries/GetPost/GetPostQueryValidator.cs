using FluentValidation;

namespace Clean.Application.Posts.Queries.GetPost
{
	public class GetPostQueryValidator : AbstractValidator<GetPostQuery>
	{
		public GetPostQueryValidator()
		{
			// * This is where we put validation for the query arguments
		}
	}
}