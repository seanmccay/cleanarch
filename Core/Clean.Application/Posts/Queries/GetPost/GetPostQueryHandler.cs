using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Clean.Application.Interfaces;
using Clean.Core.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Clean.Application.Posts.Queries.GetPost
{
	public class GetPostQueryHandler : IRequestHandler<GetPostQuery, PostDto>
	{
		private readonly IBlogContext _context;
		private readonly IMapper _mapper;

		public GetPostQueryHandler(IBlogContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}

		public async Task<PostDto> Handle(GetPostQuery request, CancellationToken cancellationToken)
		{
			return _mapper.Map<Post, PostDto>(await _context.Posts.SingleOrDefaultAsync(x => x.Id.Equals(request.Id)));
		}
	}
}