using AutoMapper;
using Clean.Core.Entities;
using Clean.Application.Interfaces;

namespace Clean.Application.Posts.Queries.GetPost
{
	public class PostDto : IHaveCustomMapping
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Body { get; set; }
		public string Author { get; set; }
		public string AuthorEmail { get; set; }


		public void CreateMappings(Profile configuration)
		{
			configuration.CreateMap<Post, PostDto>()
				.ForMember(dto => dto.Author, opt => opt.MapFrom(post => post.User.Username))
				.ForMember(dto => dto.AuthorEmail, opt => opt.MapFrom(post => post.User.Email));
		}
	}
}