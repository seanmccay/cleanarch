using FluentValidation;

namespace Clean.Application.Posts.Queries.GetPosts
{
	public class GetPostsQueryValidator : AbstractValidator<GetPostsQuery>
	{
		public GetPostsQueryValidator()
		{
			// * This is where we put validation for the query arguments
		}
	}
}