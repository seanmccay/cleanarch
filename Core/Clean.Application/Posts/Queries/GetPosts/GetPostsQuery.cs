using System.Collections.Generic;
using MediatR;

namespace Clean.Application.Posts.Queries.GetPosts
{
	public class GetPostsQuery : IRequest<IList<PostDto>>
	{
		// * Query arguments go here, this is what you expect to be coming in from the endpoint
		// * This is a get all, so nothing here
	}
}