using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Clean.Application.Interfaces;
using Clean.Core.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Clean.Application.Posts.Queries.GetPosts
{
	public class GetPostsQueryHandler : IRequestHandler<GetPostsQuery, IList<PostDto>>
	{
		private readonly IBlogContext _context;
		private readonly IMapper _mapper;

		public GetPostsQueryHandler(IBlogContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}

		public async Task<IList<PostDto>> Handle(GetPostsQuery request, CancellationToken cancellationToken)
		{
			return _mapper.Map<IList<Post>, IList<PostDto>>(await _context.Posts.ToListAsync(cancellationToken));
		}
	}
}