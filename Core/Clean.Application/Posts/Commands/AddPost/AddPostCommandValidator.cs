using Clean.Application.Posts.Commands.AddPost;
using FluentValidation;

namespace Clean.Application.Posts.Queries.GetPosts
{
	public class AddPostCommandValidator : AbstractValidator<AddPostCommand>
	{
		public AddPostCommandValidator()
		{
			// * This is where we put validation for the command arguments
		}
	}
}