using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Clean.Application.Interfaces;
using Clean.Core.Entities;
using MediatR;

namespace Clean.Application.Posts.Commands.AddPost
{
	public class AddPostCommand : IRequest, IHaveCustomMapping
	{
		// The properties that should be set for a validated post to be created
		public string Title { get; set; }
		public string Body { get; set; }
		public Guid UserId { get; set; }

		public void CreateMappings(Profile configuration)
		{
			configuration.CreateMap<AddPostCommand, Post>();
		}
	}


	// The handler that responds to the add post command
	// I guess this should do the minimum amount of work to call the task 'complete'?
	// from MediatR docs: Request/response messages [are] dispatched to a single handler
	public class Handler : IRequestHandler<AddPostCommand, Unit>
	{
		private readonly IBlogContext _context;
		private readonly IMapper _mapper;
		private readonly IMediator _mediator;

		public Handler(IBlogContext context, IMediator mediator, IMapper mapper)
		{
			_context = context;
			_mediator = mediator;
			_mapper = mapper;
		}

		public async Task<Unit> Handle(AddPostCommand request, CancellationToken cancellationToken)
		{
			// map to domain model
			Post entity = _mapper.Map<Post>(request);

			// save to database
			_context.Posts.Add(entity);
			await _context.SaveChangesAsync(cancellationToken);

			// let the notification handler know the request is complete
			await _mediator.Publish(new PostAdded { PostId = entity.Id }, cancellationToken);

			return Unit.Value;
		}
	}
}