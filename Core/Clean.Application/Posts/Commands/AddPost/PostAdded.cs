using System.Threading;
using System.Threading.Tasks;
using Clean.Application.Interfaces;
using Clean.Application.Models;
using MediatR;

namespace Clean.Application.Posts.Commands.AddPost
{
	public class PostAdded : INotification
	{
		// what the notification handler needs to get its job done
		public int PostId { get; set; }


		// from the MediatR docs: Notification messages [are] dispatched to multiple handlers
		public class PostAddedHandler : INotificationHandler<PostAdded>
		{
			private readonly INotificationService _notification;

			public PostAddedHandler(INotificationService notification)
			{
				_notification = notification;
			}

			public async Task Handle(PostAdded notification, CancellationToken cancellationToken)
			{
				// pretend we send an email after a post is added
				await _notification.SendAsync(new Message());
			}
		}
	}
}