using System;
using System.Collections.Generic;
using Clean.Core.ValueObjects;

namespace Clean.Core.Entities
{
	public class User
	{
		public User()
		{
			Posts = new HashSet<Post>();
		}

		public Guid Id { get; set; }
		public virtual Email Email { get; set; }
		public string Username { get; set; }

		public virtual IEnumerable<Post> Posts { get; private set; }
	}
}