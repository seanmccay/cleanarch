using System;

namespace Clean.Core.Entities
{
	public class Post
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Body { get; set; }
		public Guid UserId { get; set; }

		public virtual User User { get; set; }
	}
}