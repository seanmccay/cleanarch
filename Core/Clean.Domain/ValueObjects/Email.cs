using System;
using System.Collections.Generic;
using Clean.Core.Exceptions;

namespace Clean.Core.ValueObjects
{
	public class Email : ValueObject
	{
		public Email()
		{

		}

		public static Email For(string emailString)
		{
			var email = new Email();

			try
			{
				var index = emailString.IndexOf("@", StringComparison.Ordinal);
				email.Username = emailString.Substring(0, index);
				email.Domain = emailString.Substring(index + 1);
			}
			catch (Exception ex)
			{
				throw new InvalidEmailException(emailString, ex);
			}

			return email;
		}

		public string Username { get; private set; }
		public string Domain { get; private set; }

		public static implicit operator string(Email email)
		{
			return email.ToString();
		}

		public static explicit operator Email(string emailString)
		{
			return For(emailString);
		}

		public override string ToString()
		{
			return $"{Username}@{Domain}";
		}

		protected override IEnumerable<object> GetAtomicValues()
		{
			yield return Username;
			yield return Domain;
		}
	}
}