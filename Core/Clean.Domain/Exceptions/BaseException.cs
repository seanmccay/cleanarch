using System;

namespace Clean.Core.Exceptions
{
	public class BaseException : Exception
	{
		public BaseException(String message, Exception innerException) : base(message, innerException)
		{

		}
	}
}