using System;

namespace Clean.Core.Exceptions
{
	public class InvalidEmailException : Exception
	{
		public InvalidEmailException(string email, Exception innerException)
			: base($"{email} is not a valid email.", innerException)
		{

		}
	}
}