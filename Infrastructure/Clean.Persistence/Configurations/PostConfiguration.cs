using Clean.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Core.Persistence.Configurations
{
	public class PostConfiguration : IEntityTypeConfiguration<Post>
	{
		public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Post> builder)
		{
			builder.Property(e => e.Body).HasMaxLength(1000);

			builder.HasOne(d => d.User)
				.WithMany(p => p.Posts)
				.HasForeignKey(d => d.UserId);
		}
	}
}