using Clean.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Core.Persistence.Configurations
{
	public class UserConfiguration : IEntityTypeConfiguration<User>
	{
		public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<User> builder)
		{
			builder.Property(e => e.Username).HasMaxLength(25);

			builder.OwnsOne(e => e.Email); // * I guess this is required for value objects on Entities to bind correctly
		}
	}
}