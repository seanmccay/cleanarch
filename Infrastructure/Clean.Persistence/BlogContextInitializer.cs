using System;
using System.Collections.Generic;
using System.Linq;
using Clean.Core.Entities;
using Clean.Core.ValueObjects;

namespace Clean.Persistence
{
	public class BlogContextInitializer
	{
		private readonly Dictionary<int, Post> Posts = new Dictionary<int, Post>();
		private readonly Dictionary<Guid, User> Users = new Dictionary<Guid, User>();

		public static void Initialize(BlogContext context)
		{
			var initializer = new BlogContextInitializer();
			initializer.SeedEverything(context);
		}

		public void SeedEverything(BlogContext context)
		{
			context.Database.EnsureCreated();

			if (context.Users.Any())
			{
				return; // Db has been seeded
			}

			SeedUsers(context);

			SeedPosts(context);
		}

		public void SeedUsers(BlogContext context)
		{
			var users = new[]
			{
				new User { Id = new Guid("8f5a26c4-5668-4682-bc7b-a628417916dc"), Email = Email.For("wubbalubba@dubbdubb.com"), Username = "Pickle_Rick" }
			};

			context.Users.AddRange(users);

			context.SaveChanges();
		}

		public void SeedPosts(BlogContext context)
		{
			var posts = new[]
			{
				new Post { Id = 1, UserId = new Guid("8f5a26c4-5668-4682-bc7b-a628417916dc"), Title = "A Cool Blog Post", Body = "Hey, here's a cool blog. Look how hip I am" },
				new Post { Id = 2, UserId = new Guid("8f5a26c4-5668-4682-bc7b-a628417916dc"), Title = "A Cooler Blog Post", Body = "Hey, here's a cool blog. Look how hip I am" },
				new Post { Id = 3, UserId = new Guid("8f5a26c4-5668-4682-bc7b-a628417916dc"), Title = "The Coolest Blog Post", Body = "Hey, here's a cool blog. Look how hip I am" },
			};

			context.Posts.AddRange(posts);

			context.SaveChanges();
		}
	}
}
