using Clean.Application.Interfaces;
using Clean.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Clean.Persistence
{
	public class BlogContext : DbContext, IBlogContext
	{
		public BlogContext(DbContextOptions<BlogContext> options)
			: base(options)
		{
		}

		public DbSet<Post> Posts { get; set; }
		public DbSet<User> Users { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.ApplyConfigurationsFromAssembly(typeof(BlogContext).Assembly);
		}
	}
}