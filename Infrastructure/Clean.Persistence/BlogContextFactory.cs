using Microsoft.EntityFrameworkCore;
using Clean.Persistence.Infrastructure;

namespace Clean.Persistence
{
    public class NorthwindDbContextFactory : DesignTimeDbContextFactoryBase<BlogContext>
    {
        protected override BlogContext CreateNewInstance(DbContextOptions<BlogContext> options)
        {
            return new BlogContext(options);
        }
    }
}
