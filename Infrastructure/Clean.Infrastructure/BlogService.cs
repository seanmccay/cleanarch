using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clean.Application.Interfaces;
using Clean.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Clean.Infrastructure
{
	public class BlogService : IBlogService
	{
		private readonly IBlogContext _context;
		public BlogService(IBlogContext context)
		{
			_context = context;
		}

		public Task<List<Post>> GetPostsAsync()
		{
			return _context.Posts.ToListAsync();
		}
	}
}