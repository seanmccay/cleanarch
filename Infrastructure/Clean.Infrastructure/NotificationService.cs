using System.Threading.Tasks;
using Clean.Application.Interfaces;
using Clean.Application.Models;

namespace Clean.Infrastructure
{
	public class NotificationService : INotificationService
	{

		public Task SendAsync(Message message)
		{
			return Task.CompletedTask;
		}
	}
}